import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {SafeAreaView, Text, TouchableOpacity, View,Image} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {Colors} from "../../utils/colors";
import {SPACE_MD} from "../../utils/sizes";

const Header = ({title, canBack, onBack}) => {
    return (
        <SafeAreaView style={{
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%'
        }}>
            <View style={{
                paddingHorizontal: SPACE_MD,
                flexDirection: 'row'
            }}>
                <View style={{
                    width: 24,
                    justifyContent: 'center'
                }}>
                    {
                        canBack &&
                        <TouchableOpacity
                            onPress={() => onBack && onBack()}
                        >
                            <Icon
                                name="ios-person"
                                color={Colors.white}
                                size={24}
                            />
                            {/* <Image style={{width:24,height:24}} source={require('../../assets/images/add.png')}/> */}
                        </TouchableOpacity>
                    }
                </View>
                <Text style={{
                    flex: 1,
                    fontSize: 18,
                    fontWeight: '600',
                    color: Colors.white,
                    textAlign: 'center'
                }}>{title}</Text>

                <View style={{
                    width: 24,
                    justifyContent: 'center'
                }}>
                    <MaterialCommunityIcons
                        name="dots-vertical"
                        color={Colors.white}
                        size={24}
                    />
                    {/* <Image style={{width:24,height:24}} source={require('../../assets/images/add.png')}/> */}
                </View>
            </View>
        </SafeAreaView>
    );
};

Header.propTypes = {
    title: PropTypes.string,
    canBack: PropTypes.bool,
    onBack: PropTypes.func
};

export {Header};